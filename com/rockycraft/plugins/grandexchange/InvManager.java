/*   1:    */ package com.rockycraft.plugins.grandexchange;
/*   2:    */ 
/*   3:    */ import java.util.Arrays;
/*   4:    */ import java.util.List;
/*   5:    */ import java.util.logging.Level;
/*   6:    */ import java.util.logging.Logger;
/*   7:    */ import org.bukkit.Bukkit;
/*   8:    */ import org.bukkit.ChatColor;
/*   9:    */ import org.bukkit.Server;
/*  10:    */ import org.bukkit.entity.Player;
/*  11:    */ import org.bukkit.inventory.Inventory;
/*  12:    */ import org.bukkit.inventory.ItemStack;
/*  13:    */ import org.bukkit.inventory.meta.ItemMeta;
/*  14:    */ import org.bukkit.plugin.Plugin;
/*  15:    */ 
/*  16:    */ public class InvManager
/*  17:    */ {
/*  18:    */   private GrandExchange ge;
/*  19:    */   private MySQL sql;
/*  20:    */   
/*  21:    */   public InvManager(GrandExchange ge, MySQL sql)
/*  22:    */   {
/*  23: 20 */     this.ge = ge;
/*  24: 21 */     this.sql = sql;
/*  25:    */   }
/*  26:    */   
/*  27:    */   private void hotbar(Inventory i, int page)
/*  28:    */   {
/*  29: 25 */     ItemStack s = new ItemStack(this.ge.fillerID, 1);
/*  30: 26 */     ItemStack n = new ItemStack(this.ge.nextPageTypeID, 1);
/*  31: 27 */     ItemStack p = new ItemStack(this.ge.nextPageTypeID, 1);
/*  32: 28 */     ItemStack m = new ItemStack(10, 1);
/*  33: 29 */     ItemStack r = new ItemStack(119, 1);
/*  34: 30 */     int size = i.getSize();
/*  35:    */     
/*  36: 32 */     ItemMeta nIM = n.getItemMeta();
/*  37: 33 */     nIM.setDisplayName(ChatColor.DARK_GREEN + "Next page");
/*  38: 34 */     nIM.setLore(Arrays.asList(new String[] { ChatColor.GRAY + "Page: " + page }));
/*  39: 35 */     n.setItemMeta(nIM);
/*  40:    */     
/*  41: 37 */     ItemMeta pIM = p.getItemMeta();
/*  42: 38 */     pIM.setDisplayName(ChatColor.DARK_GREEN + "Previous page");
/*  43: 39 */     pIM.setLore(Arrays.asList(new String[] { ChatColor.GRAY + "Page: " + page }));
/*  44: 40 */     p.setItemMeta(pIM);
/*  45:    */     
/*  46: 42 */     ItemMeta sIM = s.getItemMeta();
/*  47: 43 */     sIM.setDisplayName(ChatColor.GRAY + "--");
/*  48: 44 */     s.setItemMeta(sIM);
/*  49:    */     
/*  50: 46 */     ItemMeta mIM = m.getItemMeta();
/*  51: 47 */     mIM.setDisplayName(ChatColor.RED + "Main menu");
/*  52: 48 */     m.setItemMeta(mIM);
/*  53:    */     
/*  54: 50 */     ItemMeta rIM = r.getItemMeta();
/*  55: 51 */     rIM.setDisplayName(ChatColor.BLUE + "Refresh");
/*  56: 52 */     rIM.setLore(Arrays.asList(new String[] { ChatColor.GRAY + "Page: " + page }));
/*  57: 53 */     r.setItemMeta(rIM);
/*  58:    */     
/*  59: 55 */     i.setItem(size - 1, n);
/*  60: 56 */     i.setItem(size - 2, s);
/*  61: 57 */     i.setItem(size - 3, s);
/*  62: 58 */     i.setItem(size - 4, r);
/*  63: 59 */     i.setItem(size - 5, m);
/*  64: 60 */     i.setItem(size - 6, r);
/*  65: 61 */     i.setItem(size - 7, s);
/*  66: 62 */     i.setItem(size - 8, s);
/*  67: 63 */     i.setItem(size - 9, p);
/*  68:    */   }
/*  69:    */   
/*  70:    */   public Inventory all(int page)
/*  71:    */   {
/*  72: 67 */     if (page < 1) {
/*  73: 68 */       page = 1;
/*  74:    */     }
/*  75: 69 */     Inventory i = this.ge.getPlugin().getServer().createInventory(null, this.ge.inventorySize, ChatColor.BLUE + "Item³: Purchase");
/*  76: 70 */     int uSize = this.ge.inventorySize - 9;
/*  77: 71 */     int start = page * this.ge.inventorySize - this.ge.inventorySize;
/*  78: 72 */     if (start > 0) {
/*  79: 73 */       start -= 9;
/*  80:    */     }
/*  81: 74 */     if (page > 2) {
/*  82: 75 */       start -= 9;
/*  83:    */     }
/*  84: 76 */     this.ge.getLogger().log(Level.INFO, "Start: " + start);
/*  85: 77 */     this.ge.getLogger().log(Level.INFO, "uSize: " + (start + uSize));
/*  86: 78 */     this.ge.getLogger().log(Level.INFO, "Page: " + page);
/*  87:    */     try
/*  88:    */     {
/*  89: 81 */       List<ItemStack> res = this.sql.queryAllSales(start, start + uSize);
/*  90: 82 */       int c = 0;
/*  91: 83 */       for (ItemStack it : res)
/*  92:    */       {
/*  93: 84 */         i.setItem(c, it);
/*  94: 85 */         c++;
/*  95:    */       }
/*  96:    */     }
/*  97:    */     catch (Exception e)
/*  98:    */     {
/*  99: 88 */       e.printStackTrace();
/* 100:    */     }
/* 101: 90 */     hotbar(i, page);
/* 102: 91 */     return i;
/* 103:    */   }
/* 104:    */   
/* 105:    */   public Inventory mainMenu()
/* 106:    */   {
/* 107: 95 */     Inventory i = this.ge.getPlugin().getServer().createInventory(null, 9, ChatColor.BLUE + "Item³: ItemCubed");
/* 108: 96 */     ItemStack cart = new ItemStack(54, 1);
/* 109: 97 */     ItemStack all = new ItemStack(47, 1);
/* 110: 98 */     ItemStack sale = new ItemStack(147, 1);
/* 111: 99 */     ItemStack bid = new ItemStack(148, 1);
/* 112:100 */     ItemStack sell = new ItemStack(266, 1);
/* 113:101 */     ItemStack auct = new ItemStack(265, 1);
/* 114:102 */     ItemStack na = new ItemStack(this.ge.fillerID, 1);
/* 115:103 */     ItemStack search = new ItemStack(345, 1);
/* 116:    */     
/* 117:105 */     ItemMeta me1 = cart.getItemMeta();
/* 118:106 */     ItemMeta me2 = cart.getItemMeta();
/* 119:107 */     ItemMeta me3 = cart.getItemMeta();
/* 120:108 */     ItemMeta me4 = cart.getItemMeta();
/* 121:109 */     ItemMeta me5 = cart.getItemMeta();
/* 122:110 */     ItemMeta me6 = cart.getItemMeta();
/* 123:111 */     ItemMeta me7 = cart.getItemMeta();
/* 124:112 */     ItemMeta me8 = cart.getItemMeta();
/* 125:113 */     ItemMeta me9 = cart.getItemMeta();
/* 126:    */     
/* 127:115 */     me1.setDisplayName(ChatColor.RESET + ChatColor.DARK_AQUA + ChatColor.BOLD + "My Wallet");
/* 128:116 */     me2.setDisplayName(ChatColor.RESET + ChatColor.DARK_GRAY + "--");
/* 129:117 */     me3.setDisplayName(ChatColor.RESET + ChatColor.GREEN + "Purchase");
/* 130:118 */     me4.setDisplayName(ChatColor.RESET + ChatColor.GREEN + "Bid");
/* 131:119 */     me5.setDisplayName(ChatColor.RESET + ChatColor.GOLD + "Sell");
/* 132:120 */     me6.setDisplayName(ChatColor.RESET + ChatColor.GOLD + "Auction");
/* 133:121 */     me7.setDisplayName(ChatColor.RESET + ChatColor.DARK_GRAY + "--");
/* 134:122 */     me8.setDisplayName(ChatColor.RESET + ChatColor.DARK_GRAY + "--");
/* 135:123 */     me9.setDisplayName(ChatColor.RESET + ChatColor.DARK_RED + ChatColor.UNDERLINE + "Search");
/* 136:    */     
/* 137:125 */     me1.setLore(Arrays.asList(new String[] { ChatColor.RESET + ChatColor.DARK_GRAY + "Your income" }));
/* 138:126 */     me4.setLore(Arrays.asList(new String[] { ChatColor.RESET + ChatColor.DARK_GRAY + "Not Yet Implemented" }));
/* 139:127 */     me6.setLore(Arrays.asList(new String[] { ChatColor.RESET + ChatColor.DARK_GRAY + "Not Yet Implemented" }));
/* 140:128 */     me9.setLore(Arrays.asList(new String[] { ChatColor.RESET + ChatColor.DARK_GRAY + "Not Yet Implemented" }));
/* 141:    */     
/* 142:130 */     cart.setItemMeta(me1);
/* 143:131 */     all.setItemMeta(me2);
/* 144:132 */     sale.setItemMeta(me3);
/* 145:133 */     bid.setItemMeta(me4);
/* 146:134 */     sell.setItemMeta(me5);
/* 147:135 */     auct.setItemMeta(me6);
/* 148:136 */     na.setItemMeta(me7);
/* 149:137 */     na.setItemMeta(me8);
/* 150:138 */     search.setItemMeta(me9);
/* 151:    */     
/* 152:140 */     i.setItem(0, cart);
/* 153:141 */     i.setItem(1, na);
/* 154:142 */     i.setItem(2, sale);
/* 155:143 */     i.setItem(3, bid);
/* 156:144 */     i.setItem(4, sell);
/* 157:145 */     i.setItem(5, auct);
/* 158:146 */     i.setItem(6, na);
/* 159:147 */     i.setItem(7, na);
/* 160:148 */     i.setItem(8, search);
/* 161:    */     
/* 162:150 */     return i;
/* 163:    */   }
/* 164:    */   
/* 165:    */   public Inventory wallet(Player p)
/* 166:    */   {
/* 167:154 */     Inventory i = Bukkit.getServer().createInventory(null, 9, ChatColor.BLUE + "Item³: Wallet");
/* 168:    */     
/* 169:156 */     ItemStack f1 = new ItemStack(this.ge.fillerID, 1);
/* 170:157 */     ItemStack f2 = new ItemStack(this.ge.cur1ID, 1);
/* 171:158 */     ItemStack f3 = new ItemStack(this.ge.nextPageTypeID, 1);
/* 172:    */     
/* 173:160 */     ItemMeta me1 = f1.getItemMeta();
/* 174:161 */     ItemMeta me2 = f1.getItemMeta();
/* 175:162 */     ItemMeta me3 = f1.getItemMeta();
/* 176:    */     
/* 177:164 */     me1.setDisplayName(ChatColor.RESET + ChatColor.DARK_GRAY + "--");
/* 178:165 */     me2.setDisplayName(ChatColor.RESET + ChatColor.GOLD + ChatColor.BOLD + "Income");
/* 179:166 */     me3.setDisplayName(ChatColor.RESET + ChatColor.DARK_RED + "Return to main menu.");
/* 180:    */     
/* 181:168 */     me2.setLore(Arrays.asList(new String[] { ChatColor.RESET + ChatColor.GREEN + "³| Money: " + this.sql.getMoney(p.getName()) }));
/* 182:    */     
/* 183:170 */     f1.setItemMeta(me1);
/* 184:171 */     f2.setItemMeta(me2);
/* 185:172 */     f3.setItemMeta(me3);
/* 186:    */     
/* 187:174 */     i.setItem(0, f1);
/* 188:175 */     i.setItem(1, f1);
/* 189:176 */     i.setItem(2, f1);
/* 190:177 */     i.setItem(3, f1);
/* 191:178 */     i.setItem(4, f2);
/* 192:179 */     i.setItem(5, f1);
/* 193:180 */     i.setItem(6, f1);
/* 194:181 */     i.setItem(7, f1);
/* 195:182 */     i.setItem(8, f3);
/* 196:    */     
/* 197:184 */     return i;
/* 198:    */   }
/* 199:    */   
/* 200:    */   public Inventory sell()
/* 201:    */   {
/* 202:188 */     Inventory i = Bukkit.getServer().createInventory(null, 9, ChatColor.BLUE + "Item³: Sell");
/* 203:    */     
/* 204:190 */     ItemStack f1 = new ItemStack(this.ge.cur01ID, 1);
/* 205:191 */     ItemStack f2 = new ItemStack(this.ge.cur01ID, 10);
/* 206:192 */     ItemStack f3 = new ItemStack(this.ge.cur1ID, 1);
/* 207:193 */     ItemStack f4 = new ItemStack(this.ge.cur10ID, 1);
/* 208:194 */     ItemStack f5 = new ItemStack(this.ge.cur100ID, 1);
/* 209:195 */     ItemStack f6 = new ItemStack(this.ge.cur100ID, 5);
/* 210:196 */     ItemStack f7 = new ItemStack(this.ge.cur100ID, 10);
/* 211:197 */     ItemStack f8 = new ItemStack(this.ge.nextPageTypeID);
/* 212:198 */     ItemStack f0 = new ItemStack(this.ge.fillerID);
/* 213:    */     
/* 214:200 */     ItemMeta me1 = f1.getItemMeta();
/* 215:201 */     ItemMeta me2 = f2.getItemMeta();
/* 216:202 */     ItemMeta me3 = f3.getItemMeta();
/* 217:203 */     ItemMeta me4 = f4.getItemMeta();
/* 218:204 */     ItemMeta me5 = f5.getItemMeta();
/* 219:205 */     ItemMeta me6 = f6.getItemMeta();
/* 220:206 */     ItemMeta me7 = f7.getItemMeta();
/* 221:207 */     ItemMeta me8 = f8.getItemMeta();
/* 222:208 */     ItemMeta me0 = f0.getItemMeta();
/* 223:    */     
/* 224:210 */     me1.setDisplayName(ChatColor.RESET + ChatColor.GOLD + "³| .01 Rocky");
/* 225:211 */     me2.setDisplayName(ChatColor.RESET + ChatColor.GOLD + "³| .1 Rocky");
/* 226:212 */     me3.setDisplayName(ChatColor.RESET + ChatColor.GOLD + "³| 1 Rocky");
/* 227:213 */     me4.setDisplayName(ChatColor.RESET + ChatColor.GOLD + "³| 10 Rockies");
/* 228:214 */     me5.setDisplayName(ChatColor.RESET + ChatColor.GOLD + "³| 100 Rockies");
/* 229:215 */     me6.setDisplayName(ChatColor.RESET + ChatColor.GOLD + "³| 500 Rockies");
/* 230:216 */     me7.setDisplayName(ChatColor.RESET + ChatColor.GOLD + "³| 1k Rockies");
/* 231:217 */     me8.setDisplayName(ChatColor.RESET + ChatColor.DARK_GREEN + ChatColor.BOLD + "³| Confirm sale");
/* 232:218 */     me0.setDisplayName(ChatColor.RESET + ChatColor.DARK_PURPLE + "³| Right-click an item to sell it");
/* 233:    */     
/* 234:220 */     me8.setLore(Arrays.asList(new String[] { ChatColor.RESET + ChatColor.GREEN + "³| Price: 0.00" }));
/* 235:    */     
/* 236:222 */     f1.setItemMeta(me1);
/* 237:223 */     f2.setItemMeta(me2);
/* 238:224 */     f3.setItemMeta(me3);
/* 239:225 */     f4.setItemMeta(me4);
/* 240:226 */     f5.setItemMeta(me5);
/* 241:227 */     f6.setItemMeta(me6);
/* 242:228 */     f7.setItemMeta(me7);
/* 243:229 */     f8.setItemMeta(me8);
/* 244:230 */     f0.setItemMeta(me0);
/* 245:    */     
/* 246:232 */     i.setItem(1, f1);
/* 247:233 */     i.setItem(2, f2);
/* 248:234 */     i.setItem(3, f3);
/* 249:235 */     i.setItem(4, f4);
/* 250:236 */     i.setItem(5, f5);
/* 251:237 */     i.setItem(6, f6);
/* 252:238 */     i.setItem(7, f7);
/* 253:239 */     i.setItem(8, f8);
/* 254:240 */     i.setItem(0, f0);
/* 255:    */     
/* 256:242 */     return i;
/* 257:    */   }
/* 258:    */ }


/* Location:           C:\Users\Ernest\Desktop\ItemCubed.jar
 * Qualified Name:     com.rockycraft.plugins.grandexchange.InvManager
 * JD-Core Version:    0.7.0.1
 */