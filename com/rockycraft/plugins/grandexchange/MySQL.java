/*   1:    */ package com.rockycraft.plugins.grandexchange;
/*   2:    */ 
/*   3:    */ import java.io.PrintStream;
/*   4:    */ import java.math.BigDecimal;
/*   5:    */ import java.sql.Connection;
/*   6:    */ import java.sql.DriverManager;
/*   7:    */ import java.sql.PreparedStatement;
/*   8:    */ import java.sql.ResultSet;
/*   9:    */ import java.sql.SQLException;
/*  10:    */ import java.sql.Timestamp;
/*  11:    */ import java.util.ArrayList;
/*  12:    */ import java.util.Arrays;
/*  13:    */ import java.util.Date;
/*  14:    */ import java.util.HashMap;
/*  15:    */ import java.util.List;
/*  16:    */ import java.util.Map;
/*  17:    */ import java.util.Set;
/*  18:    */ import net.milkbowl.vault.economy.Economy;
/*  19:    */ import org.bukkit.ChatColor;
/*  20:    */ import org.bukkit.Material;
/*  21:    */ import org.bukkit.configuration.Configuration;
/*  22:    */ import org.bukkit.enchantments.Enchantment;
/*  23:    */ import org.bukkit.inventory.ItemStack;
/*  24:    */ import org.bukkit.inventory.meta.ItemMeta;
/*  25:    */ 
/*  26:    */ public class MySQL
/*  27:    */ {
/*  28: 26 */   private GrandExchange ge = new GrandExchange();
/*  29:    */   private Configuration config;
/*  30:    */   
/*  31:    */   public MySQL(Configuration config)
/*  32:    */   {
/*  33: 30 */     this.config = config;
/*  34:    */   }
/*  35:    */   
/*  36:    */   public SQLException testDBConnection()
/*  37:    */   {
/*  38:    */     try
/*  39:    */     {
/*  40: 35 */       Connection con = DriverManager.getConnection(this.config.getString("SQLUrl"), this.config.getString("SQLUsername"), this.config.getString("SQLPassword"));
/*  41: 36 */       con.close();
/*  42:    */     }
/*  43:    */     catch (SQLException e)
/*  44:    */     {
/*  45: 38 */       return e;
/*  46:    */     }
/*  47: 40 */     return null;
/*  48:    */   }
/*  49:    */   
/*  50:    */   public void setupMySql()
/*  51:    */   {
/*  52:    */     try
/*  53:    */     {
/*  54: 45 */       Class.forName("com.mysql.jdbc.Driver");
/*  55:    */     }
/*  56:    */     catch (ClassNotFoundException e)
/*  57:    */     {
/*  58: 47 */       e.printStackTrace();
/*  59:    */     }
/*  60:    */   }
/*  61:    */   
/*  62:    */   public Connection getSQLConnection()
/*  63:    */   {
/*  64:    */     try
/*  65:    */     {
/*  66: 53 */       return DriverManager.getConnection(this.config.getString("SQLUrl"), this.config.getString("SQLUsername"), this.config.getString("SQLPassword"));
/*  67:    */     }
/*  68:    */     catch (SQLException e)
/*  69:    */     {
/*  70: 55 */       e.printStackTrace();
/*  71:    */     }
/*  72: 57 */     return null;
/*  73:    */   }
/*  74:    */   
/*  75:    */   public boolean addPlayer(String name, boolean shouldCheck)
/*  76:    */   {
/*  77:    */     try
/*  78:    */     {
/*  79: 63 */       Connection con = getSQLConnection();
/*  80: 64 */       if (shouldCheck)
/*  81:    */       {
/*  82: 65 */         String sql1 = "SELECT Name FROM grandexchange_users WHERE Name=?;";
/*  83: 66 */         PreparedStatement prStmt1 = con.prepareStatement(sql1);
/*  84: 67 */         prStmt1.setString(1, name);
/*  85: 68 */         prStmt1.execute();
/*  86:    */         
/*  87: 70 */         ResultSet rs = prStmt1.getResultSet();
/*  88: 71 */         if (rs.isBeforeFirst()) {
/*  89: 72 */           return false;
/*  90:    */         }
/*  91:    */       }
/*  92: 74 */       String sql = "INSERT INTO grandexchange_users(Name) VALUES(?);";
/*  93: 75 */       PreparedStatement prStmt = con.prepareStatement(sql);
/*  94: 76 */       prStmt.setString(1, name);
/*  95: 77 */       prStmt.executeUpdate();
/*  96: 78 */       prStmt.close();con.close();
/*  97:    */     }
/*  98:    */     catch (SQLException e)
/*  99:    */     {
/* 100: 80 */       e.printStackTrace();
/* 101:    */     }
/* 102: 82 */     return true;
/* 103:    */   }
/* 104:    */   
/* 105:    */   public boolean addItem(ItemStack item, BigDecimal price, String owner, boolean sale)
/* 106:    */   {
/* 107:    */     try
/* 108:    */     {
/* 109: 88 */       Connection con = getSQLConnection();
/* 110: 89 */       String table = "bid";
/* 111: 90 */       if (sale) {
/* 112: 91 */         table = "on_sale";
/* 113:    */       }
/* 114: 92 */       String SQL = "INSERT INTO grandexchange_" + table + 
/* 115: 93 */         "(Name, Price, Owner, McID, MatName, Enchantments, Lore, DTPlacement, Amount, Data) " + 
/* 116: 94 */         "VALUES(?,?,?,?,?,?,?,?,?,?);";
/* 117: 95 */       PreparedStatement prStmt = con.prepareStatement(SQL);
/* 118:    */       
/* 119: 97 */       String matName = item.getType().toString();
/* 120: 98 */       String name = null;
/* 121:100 */       if (item.getItemMeta().hasDisplayName()) {
/* 122:101 */         name = item.getItemMeta().getDisplayName();
/* 123:    */       }
/* 124:104 */       String enchants = null;
/* 125:105 */       if (item.getItemMeta().hasEnchants())
/* 126:    */       {
/* 127:106 */         Map<Enchantment, Integer> eM = item.getItemMeta().getEnchants();
/* 128:107 */         Set<Enchantment> eS = eM.keySet();
/* 129:108 */         for (Enchantment e : eS) {
/* 130:109 */           enchants = enchants + e.getId() + ":" + eM.get(e) + "&";
/* 131:    */         }
/* 132:111 */         enchants = enchants.substring(4);
/* 133:    */       }
/* 134:114 */       String lore = null;
/* 135:115 */       if (item.getItemMeta().hasLore())
/* 136:    */       {
/* 137:116 */         List<String> loreL = item.getItemMeta().getLore();
/* 138:117 */         for (String s : loreL) {
/* 139:118 */           lore = lore + s + ":";
/* 140:    */         }
/* 141:120 */         lore = lore.substring(4);
/* 142:    */       }
/* 143:123 */       Date d = new Date();
/* 144:124 */       Timestamp sD = new Timestamp(d.getTime());
/* 145:    */       
/* 146:126 */       String oSQL = "SELECT UserID FROM grandexchange_users WHERE Name=?;";
/* 147:127 */       PreparedStatement prStmtO = con.prepareStatement(oSQL);
/* 148:128 */       prStmtO.setString(1, owner);
/* 149:129 */       ResultSet oRS = prStmtO.executeQuery();
/* 150:130 */       oRS.next();
/* 151:131 */       int ownerID = oRS.getInt("UserID");
/* 152:132 */       prStmtO.close();
/* 153:    */       
/* 154:134 */       Integer Data = null;
/* 155:135 */       Data = Integer.valueOf(item.getDurability());
/* 156:    */       
/* 157:137 */       prStmt.setString(1, name);
/* 158:138 */       prStmt.setBigDecimal(2, price);
/* 159:139 */       prStmt.setInt(3, ownerID);
/* 160:140 */       prStmt.setInt(4, item.getTypeId());
/* 161:141 */       prStmt.setString(5, matName);
/* 162:142 */       prStmt.setString(6, enchants);
/* 163:143 */       prStmt.setString(7, lore);
/* 164:144 */       prStmt.setTimestamp(8, sD);
/* 165:145 */       prStmt.setInt(9, item.getAmount());
/* 166:146 */       prStmt.setInt(10, Data.intValue());
/* 167:    */       
/* 168:148 */       prStmt.executeUpdate();
/* 169:    */       
/* 170:150 */       prStmt.close();
/* 171:151 */       con.close();
/* 172:    */     }
/* 173:    */     catch (SQLException e)
/* 174:    */     {
/* 175:153 */       e.printStackTrace();
/* 176:154 */       return false;
/* 177:    */     }
/* 178:156 */     return true;
/* 179:    */   }
/* 180:    */   
/* 181:    */   public List<ItemStack> queryAllSales(int start, int limit)
/* 182:    */   {
/* 183:161 */     List<ItemStack> list = new ArrayList();
/* 184:    */     try
/* 185:    */     {
/* 186:163 */       Connection con = getSQLConnection();
/* 187:164 */       String sql = "SELECT * FROM grandexchange_on_sale WHERE Sold=0 ORDER BY DTPlacement DESC LIMIT ?,?;";
/* 188:165 */       PreparedStatement prStmt = con.prepareStatement(sql);
/* 189:166 */       prStmt.setInt(1, start);
/* 190:167 */       prStmt.setInt(2, limit);
/* 191:168 */       if (prStmt.execute())
/* 192:    */       {
/* 193:169 */         ResultSet rs = prStmt.getResultSet();
/* 194:170 */         String nQS = "SELECT Name FROM grandexchange_users WHERE UserID=?;";
/* 195:171 */         String owner = null;
/* 196:172 */         Map<Enchantment, Integer> ench = new HashMap();
/* 197:173 */         while (rs.next())
/* 198:    */         {
/* 199:174 */           ItemStack is = new ItemStack(1, 1);
/* 200:175 */           List<String> lore = new ArrayList();
/* 201:    */           
/* 202:177 */           String name = rs.getString("Name");
/* 203:178 */           String exlo = rs.getString("Lore");
/* 204:179 */           String enchantments = rs.getString("Enchantments");
/* 205:180 */           BigDecimal price = rs.getBigDecimal("Price");
/* 206:181 */           Integer amount = Integer.valueOf(rs.getInt("Amount"));
/* 207:182 */           Integer id = Integer.valueOf(rs.getInt("Owner"));
/* 208:183 */           Integer McID = Integer.valueOf(rs.getInt("McID"));
/* 209:184 */           Integer uID = Integer.valueOf(rs.getInt("ID"));
/* 210:185 */           int data = rs.getInt("Data");
/* 211:186 */           ItemMeta im = is.getItemMeta();
/* 212:189 */           if (name != null) {
/* 213:190 */             im.setDisplayName(name.trim());
/* 214:    */           }
/* 215:191 */           lore.add(ChatColor.RESET + ChatColor.UNDERLINE + ChatColor.GREEN + "³| Price: " + price);
/* 216:192 */           is.setAmount(amount.intValue());
/* 217:193 */           is.setTypeId(McID.intValue());
/* 218:    */           
/* 219:    */ 
/* 220:196 */           PreparedStatement nQ = con.prepareStatement(nQS);
/* 221:197 */           nQ.setInt(1, id.intValue());
/* 222:198 */           nQ.execute();
/* 223:199 */           ResultSet qs = nQ.getResultSet();
/* 224:200 */           if (nQ.execute()) {
/* 225:201 */             while (qs.next()) {
/* 226:202 */               owner = qs.getString("Name");
/* 227:    */             }
/* 228:    */           }
/* 229:205 */           nQ.close();
/* 230:206 */           lore.add(ChatColor.RESET + ChatColor.BLUE + "³| Owner: " + owner);
/* 231:209 */           if (exlo != null) {
/* 232:210 */             lore.addAll(Arrays.asList(exlo.split(":")));
/* 233:    */           }
/* 234:214 */           if (enchantments != null)
/* 235:    */           {
/* 236:    */             String[] sa;
/* 237:215 */             for (String s : Arrays.asList(enchantments.split("&"))) {
/* 238:216 */               if (s != null)
/* 239:    */               {
/* 240:217 */                 sa = s.split(":");
/* 241:218 */                 ench.put(Enchantment.getById(Integer.parseInt(sa[0])), Integer.valueOf(Integer.parseInt(sa[1])));
/* 242:    */               }
/* 243:    */             }
/* 244:221 */             Set<Enchantment> enchS = ench.keySet();
/* 245:222 */             for (Enchantment enchant : enchS) {
/* 246:223 */               im.addEnchant(enchant, ((Integer)ench.get(enchant)).intValue(), true);
/* 247:    */             }
/* 248:    */           }
/* 249:227 */           lore.add(ChatColor.RESET + ChatColor.BLACK + "³| ID: " + uID);
/* 250:    */           
/* 251:229 */           is.setDurability((short)data);
/* 252:230 */           im.setLore(lore);
/* 253:231 */           is.setItemMeta(im);
/* 254:232 */           list.add(is);
/* 255:    */         }
/* 256:    */       }
/* 257:235 */       prStmt.close();con.close();
/* 258:    */     }
/* 259:    */     catch (SQLException e)
/* 260:    */     {
/* 261:237 */       e.printStackTrace();
/* 262:238 */       System.out.println(e.getMessage());
/* 263:239 */       return null;
/* 264:    */     }
/* 265:241 */     return list;
/* 266:    */   }
/* 267:    */   
/* 268:    */   public boolean setSold(int id, String buyer, String table)
/* 269:    */   {
/* 270:    */     try
/* 271:    */     {
/* 272:247 */       System.out.println("Set Sold.");
/* 273:    */       
/* 274:249 */       Connection con = getSQLConnection();
/* 275:250 */       String sqlO = "SELECT UserID from grandexchange_users WHERE Name=?;";
/* 276:251 */       String sqlP = "SELECT Price FROM grandexchange_" + table + " WHERE ID=?;";
/* 277:252 */       String sql = "UPDATE grandexchange_" + table + " SET Sold=1, Buyer=? WHERE ID=?;";
/* 278:253 */       String sql1 = "UPDATE grandexchange_users SET Bought=(SELECT Bought)+1 WHERE Name=?;";
/* 279:254 */       String sql2 = "UPDATE grandexchange_users SET Money=(SELECT Money)+? WHERE UserID=(SELECT Owner FROM grandexchange_" + 
/* 280:    */       
/* 281:256 */         table + " WHERE ID=?);";
/* 282:    */       
/* 283:258 */       PreparedStatement prStmtO = con.prepareStatement(sqlO);
/* 284:259 */       PreparedStatement prStmtP = con.prepareStatement(sqlP);
/* 285:260 */       PreparedStatement prStmt = con.prepareStatement(sql);
/* 286:261 */       PreparedStatement prStmt1 = con.prepareStatement(sql1);
/* 287:262 */       PreparedStatement prStmt2 = con.prepareStatement(sql2);
/* 288:    */       
/* 289:264 */       prStmtO.setString(1, buyer);
/* 290:265 */       ResultSet rs1 = prStmtO.executeQuery();
/* 291:266 */       rs1.next();
/* 292:267 */       int UserID = rs1.getInt(1);
/* 293:268 */       prStmtO.close();
/* 294:    */       
/* 295:270 */       prStmtP.setInt(1, id);
/* 296:271 */       ResultSet rs2 = prStmtP.executeQuery();
/* 297:272 */       rs2.next();
/* 298:273 */       BigDecimal price = rs2.getBigDecimal(1);
/* 299:274 */       prStmtP.close();
/* 300:    */       
/* 301:276 */       BigDecimal tax = price.divide(new BigDecimal(100));
/* 302:277 */       Economy economy = GrandExchange.economy;
/* 303:278 */       economy.bankDeposit(this.ge.federalBank, tax.doubleValue());
/* 304:279 */       price = price.subtract(tax);
/* 305:    */       
/* 306:281 */       prStmt.setInt(1, UserID);
/* 307:282 */       prStmt.setInt(2, id);
/* 308:283 */       System.out.println(prStmt.toString());
/* 309:284 */       prStmt.executeUpdate();
/* 310:285 */       prStmt.close();
/* 311:    */       
/* 312:287 */       prStmt1.setString(1, buyer);
/* 313:288 */       System.out.println(prStmt1.toString());
/* 314:289 */       prStmt1.executeUpdate();
/* 315:290 */       prStmt1.close();
/* 316:    */       
/* 317:292 */       prStmt2.setBigDecimal(1, price);
/* 318:293 */       prStmt2.setInt(2, id);
/* 319:294 */       System.out.println(prStmt2.toString());
/* 320:295 */       prStmt2.executeUpdate();
/* 321:296 */       prStmt2.close();
/* 322:    */       
/* 323:298 */       con.close();
/* 324:299 */       return true;
/* 325:    */     }
/* 326:    */     catch (Exception e)
/* 327:    */     {
/* 328:301 */       e.printStackTrace();
/* 329:    */     }
/* 330:302 */     return false;
/* 331:    */   }
/* 332:    */   
/* 333:    */   public double getMoney(String name)
/* 334:    */   {
/* 335:308 */     double money = 0.0D;
/* 336:    */     try
/* 337:    */     {
/* 338:311 */       Connection con = getSQLConnection();
/* 339:312 */       String sql = "SELECT Money FROM grandexchange_users WHERE Name=?;";
/* 340:313 */       PreparedStatement prStmt = con.prepareStatement(sql);
/* 341:    */       
/* 342:315 */       prStmt.setString(1, name);
/* 343:316 */       ResultSet rs = prStmt.executeQuery();
/* 344:317 */       rs.next();
/* 345:318 */       money = rs.getDouble("Money");
/* 346:    */       
/* 347:320 */       prStmt.close();
/* 348:321 */       con.close();
/* 349:    */     }
/* 350:    */     catch (SQLException e)
/* 351:    */     {
/* 352:323 */       e.printStackTrace();
/* 353:    */     }
/* 354:325 */     return money;
/* 355:    */   }
/* 356:    */   
/* 357:    */   public double claimMoney(String name)
/* 358:    */   {
/* 359:330 */     double money = 0.0D;
/* 360:    */     try
/* 361:    */     {
/* 362:333 */       Connection con = getSQLConnection();
/* 363:    */       
/* 364:335 */       String sql1 = "SELECT Money FROM grandexchange_users WHERE Name=?;";
/* 365:336 */       String sql = "UPDATE grandexchange_users SET Money=0 WHERE Name=?;";
/* 366:    */       
/* 367:338 */       PreparedStatement prStmt1 = con.prepareStatement(sql1);
/* 368:339 */       PreparedStatement prStmt = con.prepareStatement(sql);
/* 369:    */       
/* 370:341 */       prStmt1.setString(1, name);
/* 371:342 */       ResultSet rs = prStmt1.executeQuery();
/* 372:343 */       rs.next();
/* 373:344 */       money = rs.getDouble("Money");
/* 374:345 */       prStmt1.close();
/* 375:    */       
/* 376:347 */       prStmt.setString(1, name);
/* 377:348 */       prStmt.executeUpdate();
/* 378:    */       
/* 379:350 */       prStmt.close();
/* 380:351 */       con.close();
/* 381:    */     }
/* 382:    */     catch (SQLException e)
/* 383:    */     {
/* 384:353 */       e.printStackTrace();
/* 385:    */     }
/* 386:355 */     return money;
/* 387:    */   }
/* 388:    */   
/* 389:    */   public boolean isSold(int id, String table)
/* 390:    */   {
/* 391:360 */     String sql = "SELECT Sold FROM grandexchange_" + table + " WHERE ID=?;";
/* 392:361 */     int sold = 0;
/* 393:    */     try
/* 394:    */     {
/* 395:363 */       Connection con = getSQLConnection();
/* 396:    */       
/* 397:365 */       PreparedStatement prStmt = con.prepareStatement(sql);
/* 398:366 */       prStmt.setInt(1, id);
/* 399:    */       
/* 400:368 */       ResultSet rs = prStmt.executeQuery();
/* 401:369 */       rs.next();
/* 402:370 */       sold = rs.getInt("Sold");
/* 403:    */       
/* 404:372 */       prStmt.close();
/* 405:373 */       con.close();
/* 406:    */     }
/* 407:    */     catch (SQLException exc)
/* 408:    */     {
/* 409:375 */       exc.printStackTrace();
/* 410:    */     }
/* 411:378 */     if (sold == 1) {
/* 412:379 */       return true;
/* 413:    */     }
/* 414:381 */     return false;
/* 415:    */   }
/* 416:    */ }


/* Location:           C:\Users\Ernest\Desktop\ItemCubed.jar
 * Qualified Name:     com.rockycraft.plugins.grandexchange.MySQL
 * JD-Core Version:    0.7.0.1
 */