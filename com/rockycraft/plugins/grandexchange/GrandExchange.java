/*   1:    */ package com.rockycraft.plugins.grandexchange;
/*   2:    */ 
/*   3:    */ import java.io.PrintStream;
/*   4:    */ import java.sql.SQLException;
/*   5:    */ import java.util.logging.Level;
/*   6:    */ import java.util.logging.Logger;
/*   7:    */ import net.milkbowl.vault.economy.Economy;
/*   8:    */ import org.bukkit.Bukkit;
/*   9:    */ import org.bukkit.Server;
/*  10:    */ import org.bukkit.command.Command;
/*  11:    */ import org.bukkit.command.CommandSender;
/*  12:    */ import org.bukkit.configuration.file.FileConfiguration;
/*  13:    */ import org.bukkit.entity.Player;
/*  14:    */ import org.bukkit.event.Listener;
/*  15:    */ import org.bukkit.plugin.Plugin;
/*  16:    */ import org.bukkit.plugin.PluginManager;
/*  17:    */ import org.bukkit.plugin.RegisteredServiceProvider;
/*  18:    */ import org.bukkit.plugin.ServicesManager;
/*  19:    */ import org.bukkit.plugin.java.JavaPlugin;
/*  20:    */ 
/*  21:    */ public class GrandExchange
/*  22:    */   extends JavaPlugin
/*  23:    */   implements Listener
/*  24:    */ {
/*  25:    */   private MySQL sql;
/*  26:    */   private Plugin plugin;
/*  27:    */   private InvManager im;
/*  28:    */   public static Economy economy;
/*  29:    */   public int inventorySize;
/*  30:    */   public int nextPageTypeID;
/*  31:    */   public int fillerID;
/*  32:    */   public int cur100ID;
/*  33:    */   public int cur10ID;
/*  34:    */   public int cur1ID;
/*  35:    */   public int cur01ID;
/*  36: 29 */   public String federalBank = "FederalHoarder";
/*  37:    */   
/*  38:    */   private boolean setupEconomy()
/*  39:    */   {
/*  40: 32 */     if (getServer().getPluginManager().getPlugin("Vault") == null) {
/*  41: 33 */       return false;
/*  42:    */     }
/*  43: 35 */     RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
/*  44: 36 */     if (rsp == null) {
/*  45: 37 */       return false;
/*  46:    */     }
/*  47: 39 */     economy = (Economy)rsp.getProvider();
/*  48: 40 */     return economy != null;
/*  49:    */   }
/*  50:    */   
/*  51:    */   public void onDisable() {}
/*  52:    */   
/*  53:    */   public void onEnable()
/*  54:    */   {
/*  55: 48 */     if (!setupEconomy()) {
/*  56: 49 */       System.out.println("SHIT BE BROKEN");
/*  57:    */     }
/*  58: 51 */     System.out.println(economy);
/*  59: 52 */     getServer().getPluginManager().registerEvents(this, this);
/*  60:    */     
/*  61: 54 */     this.nextPageTypeID = getConfig().getInt("NextPageTypeID");
/*  62: 55 */     this.inventorySize = getConfig().getInt("InventorySize");
/*  63: 56 */     this.fillerID = getConfig().getInt("FillerID");
/*  64: 57 */     this.cur01ID = getConfig().getInt("01CurID");
/*  65: 58 */     this.cur1ID = getConfig().getInt("1CurID");
/*  66: 59 */     this.cur10ID = getConfig().getInt("10CurID");
/*  67: 60 */     this.cur100ID = getConfig().getInt("100CurID");
/*  68: 61 */     this.federalBank = getConfig().getString("FederalBank");
/*  69:    */     
/*  70: 63 */     this.sql = new MySQL(getConfig());
/*  71: 64 */     this.sql.setupMySql();
/*  72: 65 */     if (this.sql.testDBConnection() != null) {
/*  73: 66 */       getLogger().log(Level.SEVERE, "DB Connection: " + this.sql.testDBConnection().getMessage());
/*  74:    */     }
/*  75: 68 */     this.plugin = this;
/*  76: 69 */     this.im = new InvManager(this, this.sql);
/*  77: 70 */     new ButtonHandler(this, this.im, this.sql);
/*  78:    */   }
/*  79:    */   
/*  80:    */   public Plugin getPlugin()
/*  81:    */   {
/*  82: 74 */     return this.plugin;
/*  83:    */   }
/*  84:    */   
/*  85:    */   public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
/*  86:    */   {
/*  87: 78 */     if ((cmd.getName().equalsIgnoreCase("ic")) || (cmd.getName().equalsIgnoreCase("itemcubed")) || 
/*  88: 79 */       (cmd.getName().equalsIgnoreCase("i3")) || (cmd.getName().equalsIgnoreCase("i")))
/*  89:    */     {
/*  90: 80 */       if (sender.hasPermission("itemcubed.general"))
/*  91:    */       {
/*  92: 81 */         this.sql.addPlayer(sender.getName(), true);
/*  93: 82 */         String d = "";
/*  94: 83 */         if (args.length == 0) {
/*  95: 84 */           d = "main";
/*  96:    */         } else {
/*  97: 86 */           d = args[0];
/*  98:    */         }
/*  99: 88 */         Player p = Bukkit.getPlayer(sender.getName());
/* 100: 89 */         if (d.equalsIgnoreCase("purchase"))
/* 101:    */         {
/* 102: 90 */           int pa = 1;
/* 103: 91 */           if (args.length > 1) {
/* 104:    */             try
/* 105:    */             {
/* 106: 93 */               pa = Integer.parseInt(args[1]);
/* 107: 94 */               if (pa <= 0) {
/* 108: 95 */                 pa = 1;
/* 109:    */               }
/* 110:    */             }
/* 111:    */             catch (Exception ex)
/* 112:    */             {
/* 113: 98 */               sender.sendMessage("The page argument has to be a number.");
/* 114:    */             }
/* 115:    */           }
/* 116:101 */           p.openInventory(this.im.all(pa));
/* 117:    */         }
/* 118:102 */         else if (d.equalsIgnoreCase("main"))
/* 119:    */         {
/* 120:103 */           p.openInventory(this.im.mainMenu());
/* 121:    */         }
/* 122:    */       }
/* 123:    */       else
/* 124:    */       {
/* 125:106 */         sender.sendMessage("NOT OP");
/* 126:    */       }
/* 127:108 */       return true;
/* 128:    */     }
/* 129:110 */     return false;
/* 130:    */   }
/* 131:    */ }


/* Location:           C:\Users\Ernest\Desktop\ItemCubed.jar
 * Qualified Name:     com.rockycraft.plugins.grandexchange.GrandExchange
 * JD-Core Version:    0.7.0.1
 */