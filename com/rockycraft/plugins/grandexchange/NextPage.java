/*  1:   */ package com.rockycraft.plugins.grandexchange;
/*  2:   */ 
/*  3:   */ import org.bukkit.entity.Player;
/*  4:   */ import org.bukkit.inventory.Inventory;
/*  5:   */ import org.bukkit.scheduler.BukkitRunnable;
/*  6:   */ 
/*  7:   */ public class NextPage
/*  8:   */   extends BukkitRunnable
/*  9:   */ {
/* 10:   */   private Player p;
/* 11:   */   private Inventory i;
/* 12:   */   
/* 13:   */   public NextPage(Player p, Inventory i)
/* 14:   */   {
/* 15:13 */     this.p = p;
/* 16:14 */     this.i = i;
/* 17:   */   }
/* 18:   */   
/* 19:   */   public NextPage(Player p)
/* 20:   */   {
/* 21:17 */     this.p = p;
/* 22:   */   }
/* 23:   */   
/* 24:   */   public void run()
/* 25:   */   {
/* 26:22 */     this.p.closeInventory();
/* 27:23 */     if (this.i != null) {
/* 28:24 */       this.p.openInventory(this.i);
/* 29:   */     }
/* 30:   */   }
/* 31:   */ }


/* Location:           C:\Users\Ernest\Desktop\ItemCubed.jar
 * Qualified Name:     com.rockycraft.plugins.grandexchange.NextPage
 * JD-Core Version:    0.7.0.1
 */