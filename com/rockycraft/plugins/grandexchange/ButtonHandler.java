/*   1:    */ package com.rockycraft.plugins.grandexchange;
/*   2:    */ 
/*   3:    */ import java.math.BigDecimal;
/*   4:    */ import java.util.Arrays;
/*   5:    */ import java.util.Iterator;
/*   6:    */ import java.util.List;
/*   7:    */ import net.milkbowl.vault.economy.Economy;
/*   8:    */ import org.bukkit.ChatColor;
/*   9:    */ import org.bukkit.Location;
/*  10:    */ import org.bukkit.Server;
/*  11:    */ import org.bukkit.World;
/*  12:    */ import org.bukkit.entity.HumanEntity;
/*  13:    */ import org.bukkit.entity.Player;
/*  14:    */ import org.bukkit.event.EventHandler;
/*  15:    */ import org.bukkit.event.EventPriority;
/*  16:    */ import org.bukkit.event.Listener;
/*  17:    */ import org.bukkit.event.inventory.InventoryClickEvent;
/*  18:    */ import org.bukkit.event.inventory.InventoryCloseEvent;
/*  19:    */ import org.bukkit.inventory.Inventory;
/*  20:    */ import org.bukkit.inventory.ItemStack;
/*  21:    */ import org.bukkit.inventory.PlayerInventory;
/*  22:    */ import org.bukkit.inventory.meta.ItemMeta;
/*  23:    */ import org.bukkit.plugin.PluginManager;
/*  24:    */ 
/*  25:    */ public class ButtonHandler
/*  26:    */   implements Listener
/*  27:    */ {
/*  28:    */   private GrandExchange ge;
/*  29:    */   private InvManager im;
/*  30:    */   private MySQL sql;
/*  31:    */   
/*  32:    */   public ButtonHandler(GrandExchange ge, InvManager im, MySQL sql)
/*  33:    */   {
/*  34: 26 */     this.ge = ge;
/*  35: 27 */     this.im = im;
/*  36: 28 */     this.sql = sql;
/*  37: 29 */     ge.getServer().getPluginManager().registerEvents(this, ge);
/*  38:    */   }
/*  39:    */   
/*  40:    */   @EventHandler
/*  41:    */   public void onInventoryClickEvent(InventoryClickEvent e)
/*  42:    */   {
/*  43: 35 */     Inventory inv = e.getInventory();
/*  44: 36 */     String title = inv.getName();
/*  45: 38 */     if (title.contains("Item³"))
/*  46:    */     {
/*  47: 39 */       ItemStack is = e.getCurrentItem();
/*  48: 40 */       ItemMeta me = is.getItemMeta();
/*  49: 41 */       Player p = (Player)e.getWhoClicked();
/*  50: 43 */       if ((title.toLowerCase().contains("purchase")) && (is != null))
/*  51:    */       {
/*  52: 44 */         if (e.getRawSlot() < this.ge.inventorySize - 9)
/*  53:    */         {
/*  54: 45 */           List<String> curLore = me.getLore();
/*  55: 46 */           boolean lS = false;
/*  56: 47 */           for (String s : curLore) {
/*  57: 48 */             if ((s.toLowerCase().contains("purchase")) && (s.contains("³"))) {
/*  58: 49 */               lS = true;
/*  59:    */             }
/*  60:    */           }
/*  61: 52 */           if (lS)
/*  62:    */           {
/*  63: 53 */             BigDecimal price = new BigDecimal(0.0D);
/*  64: 54 */             for (String s : curLore) {
/*  65: 55 */               if ((s.toLowerCase().contains("price")) && (s.contains("³")))
/*  66:    */               {
/*  67: 56 */                 String[] ss = s.split(":");
/*  68: 57 */                 price = new BigDecimal(ss[1].trim());
/*  69:    */               }
/*  70:    */             }
/*  71: 60 */             if (GrandExchange.economy.getBalance(p.getName()) >= price.doubleValue())
/*  72:    */             {
/*  73: 61 */               if (p.getInventory().firstEmpty() != -1)
/*  74:    */               {
/*  75: 62 */                 int uID = -1;
/*  76: 63 */                 Object curIt = curLore.iterator();
/*  77: 64 */                 while (((Iterator)curIt).hasNext())
/*  78:    */                 {
/*  79: 65 */                   String s = (String)((Iterator)curIt).next();
/*  80: 66 */                   if ((s.toLowerCase().contains("id")) && (s.contains("³")))
/*  81:    */                   {
/*  82: 67 */                     String[] ss = s.split(":");
/*  83: 68 */                     uID = Integer.parseInt(ss[1].trim());
/*  84:    */                   }
/*  85: 70 */                   if (s.contains("³")) {
/*  86: 71 */                     ((Iterator)curIt).remove();
/*  87:    */                   }
/*  88:    */                 }
/*  89: 73 */                 if (!this.sql.isSold(uID, "on_sale"))
/*  90:    */                 {
/*  91: 74 */                   me.setLore(curLore);
/*  92: 75 */                   is.setItemMeta(me);
/*  93: 76 */                   p.getInventory().addItem(new ItemStack[] { is });
/*  94: 77 */                   GrandExchange.economy.withdrawPlayer(p.getName(), price.doubleValue());
/*  95: 78 */                   this.sql.setSold(uID, p.getName(), "on_sale");
/*  96: 79 */                   new NextPage(p, this.im.mainMenu()).runTask(this.ge);
/*  97:    */                 }
/*  98:    */                 else
/*  99:    */                 {
/* 100: 81 */                   curLore.add(ChatColor.RESET + ChatColor.DARK_RED + "³| Already sold.");
/* 101: 82 */                   me.setLore(curLore);
/* 102:    */                 }
/* 103:    */               }
/* 104:    */               else
/* 105:    */               {
/* 106: 85 */                 curLore.add(ChatColor.RESET + ChatColor.DARK_RED + "³| No space in inventory.");
/* 107: 86 */                 me.setLore(curLore);
/* 108:    */               }
/* 109:    */             }
/* 110:    */             else
/* 111:    */             {
/* 112: 89 */               curLore.add(ChatColor.RESET + ChatColor.DARK_RED + "³| Not enough money.");
/* 113: 90 */               me.setLore(curLore);
/* 114:    */             }
/* 115:    */           }
/* 116:    */           else
/* 117:    */           {
/* 118: 93 */             curLore.add(ChatColor.RESET + ChatColor.DARK_GREEN + "³| Click again to purchase.");
/* 119: 94 */             p.sendMessage(curLore.toString());
/* 120: 95 */             me.setLore(curLore);
/* 121:    */           }
/* 122:    */         }
/* 123: 97 */         else if (e.getRawSlot() <= this.ge.inventorySize)
/* 124:    */         {
/* 125: 98 */           if (is.getItemMeta().getDisplayName().toLowerCase().contains("next page"))
/* 126:    */           {
/* 127: 99 */             String l = (String)is.getItemMeta().getLore().get(0);
/* 128:100 */             String[] pages = l.split(": ");
/* 129:101 */             Integer page = Integer.valueOf(Integer.parseInt(pages[1].trim()));
/* 130:102 */             new NextPage(p, this.im.all(page.intValue() + 1)).runTask(this.ge);
/* 131:    */           }
/* 132:103 */           else if (is.getItemMeta().getDisplayName().toLowerCase().contains("previous page"))
/* 133:    */           {
/* 134:104 */             String l = (String)is.getItemMeta().getLore().get(0);
/* 135:105 */             String[] pages = l.split(": ");
/* 136:106 */             Integer page = Integer.valueOf(Integer.parseInt(pages[1].trim()));
/* 137:107 */             new NextPage(p, this.im.all(page.intValue() - 1)).runTask(this.ge);
/* 138:    */           }
/* 139:108 */           else if (is.getItemMeta().getDisplayName().toLowerCase().contains("refresh"))
/* 140:    */           {
/* 141:109 */             String l = (String)is.getItemMeta().getLore().get(0);
/* 142:110 */             String[] pages = l.split(": ");
/* 143:111 */             Integer page = Integer.valueOf(Integer.parseInt(pages[1].trim()));
/* 144:112 */             new NextPage(p, this.im.all(page.intValue())).runTask(this.ge);
/* 145:    */           }
/* 146:113 */           else if (is.getItemMeta().getDisplayName().toLowerCase().contains("main menu"))
/* 147:    */           {
/* 148:114 */             new NextPage(p, this.im.mainMenu()).runTask(this.ge);
/* 149:    */           }
/* 150:    */         }
/* 151:    */       }
/* 152:117 */       else if ((title.toLowerCase().contains("itemcubed")) && (is != null))
/* 153:    */       {
/* 154:118 */         String name = is.getItemMeta().getDisplayName().toLowerCase();
/* 155:119 */         if (name.contains("wallet")) {
/* 156:120 */           new NextPage(p, this.im.wallet(p)).runTask(this.ge);
/* 157:121 */         } else if (name.contains("purchase")) {
/* 158:122 */           new NextPage(p, this.im.all(1)).runTask(this.ge);
/* 159:123 */         } else if (name.contains("sell")) {
/* 160:124 */           new NextPage(p, this.im.sell()).runTask(this.ge);
/* 161:    */         }
/* 162:    */       }
/* 163:127 */       else if ((title.toLowerCase().contains("wallet")) && (is != null))
/* 164:    */       {
/* 165:128 */         if (is.getItemMeta().getDisplayName().toLowerCase().contains("menu"))
/* 166:    */         {
/* 167:129 */           new NextPage(p, this.im.mainMenu()).runTask(this.ge);
/* 168:    */         }
/* 169:130 */         else if (is.getItemMeta().getDisplayName().toLowerCase().contains("income"))
/* 170:    */         {
/* 171:131 */           double price = this.sql.claimMoney(p.getName());
/* 172:132 */           GrandExchange.economy.depositPlayer(p.getName(), price);
/* 173:133 */           if (price == 1.0D) {
/* 174:134 */             p.sendMessage(ChatColor.AQUA + "1 " + GrandExchange.economy.currencyNameSingular() + ChatColor.RESET + ChatColor.GREEN + " has been added to your account.");
/* 175:    */           } else {
/* 176:136 */             p.sendMessage(ChatColor.AQUA + price + " " + GrandExchange.economy.currencyNamePlural() + ChatColor.RESET + ChatColor.GREEN + " have been added to your account.");
/* 177:    */           }
/* 178:137 */           new NextPage(p, this.im.mainMenu()).runTask(this.ge);
/* 179:    */         }
/* 180:    */       }
/* 181:139 */       else if ((title.toLowerCase().contains("sell")) && (is != null))
/* 182:    */       {
/* 183:140 */         boolean noButton = false;
/* 184:141 */         if (is.hasItemMeta())
/* 185:    */         {
/* 186:142 */           if (is.getItemMeta().hasDisplayName())
/* 187:    */           {
/* 188:143 */             String name = is.getItemMeta().getDisplayName().toLowerCase();
/* 189:144 */             if ((name.contains("³")) && (!name.contains(ChatColor.ITALIC)))
/* 190:    */             {
/* 191:145 */               if (e.getSlot() > 0)
/* 192:    */               {
/* 193:146 */                 ItemMeta me2 = inv.getItem(8).getItemMeta();
/* 194:147 */                 Object lore = me2.getLore();
/* 195:148 */                 String[] ss = ((String)((List)lore).get(0)).split(":");
/* 196:149 */                 BigDecimal price = new BigDecimal(ss[1].trim());
/* 197:150 */                 boolean changePrice = true;
/* 198:151 */                 if (name.contains("confirm"))
/* 199:    */                 {
/* 200:152 */                   if ((inv.getItem(0).getItemMeta().getDisplayName() != null) && 
/* 201:153 */                     (inv.getItem(0).getItemMeta().getDisplayName().contains("³"))) {
/* 202:154 */                     return;
/* 203:    */                   }
/* 204:157 */                   this.sql.addItem(inv.getItem(0), price, p.getName(), true);
/* 205:158 */                   inv.clear(0);
/* 206:159 */                   new NextPage(p, this.im.mainMenu()).runTask(this.ge);
/* 207:160 */                   changePrice = false;
/* 208:    */                 }
/* 209:161 */                 else if (name.contains(" .01 "))
/* 210:    */                 {
/* 211:162 */                   price = price.add(new BigDecimal("0.01"));
/* 212:    */                 }
/* 213:163 */                 else if (name.contains(" .1 "))
/* 214:    */                 {
/* 215:164 */                   price = price.add(new BigDecimal("0.1"));
/* 216:    */                 }
/* 217:165 */                 else if (name.contains(" 1 "))
/* 218:    */                 {
/* 219:166 */                   price = price.add(new BigDecimal(1));
/* 220:    */                 }
/* 221:167 */                 else if (name.contains(" 10 "))
/* 222:    */                 {
/* 223:168 */                   price = price.add(new BigDecimal(10));
/* 224:    */                 }
/* 225:169 */                 else if (name.contains(" 100 "))
/* 226:    */                 {
/* 227:170 */                   price = price.add(new BigDecimal(100));
/* 228:    */                 }
/* 229:171 */                 else if (name.contains(" 500 "))
/* 230:    */                 {
/* 231:172 */                   price = price.add(new BigDecimal(500));
/* 232:    */                 }
/* 233:173 */                 else if (name.contains(" 1k "))
/* 234:    */                 {
/* 235:174 */                   price = price.add(new BigDecimal(1000));
/* 236:    */                 }
/* 237:176 */                 if (changePrice)
/* 238:    */                 {
/* 239:177 */                   me2.setLore(Arrays.asList(new String[] { ss[0] + ": " + price }));
/* 240:178 */                   inv.getItem(8).setItemMeta(me2);
/* 241:    */                 }
/* 242:    */               }
/* 243:    */             }
/* 244:    */             else {
/* 245:182 */               noButton = true;
/* 246:    */             }
/* 247:    */           }
/* 248:    */           else
/* 249:    */           {
/* 250:185 */             noButton = true;
/* 251:    */           }
/* 252:    */         }
/* 253:    */         else {
/* 254:188 */           noButton = true;
/* 255:    */         }
/* 256:190 */         if ((noButton) && 
/* 257:191 */           (e.isRightClick())) {
/* 258:    */           try
/* 259:    */           {
/* 260:193 */             boolean noDisName = true;
/* 261:194 */             if ((inv.getItem(0).hasItemMeta()) && 
/* 262:195 */               (inv.getItem(0).getItemMeta().hasDisplayName()) && 
/* 263:196 */               (inv.getItem(0).getItemMeta().getDisplayName().contains("³")))
/* 264:    */             {
/* 265:197 */               inv.setItem(0, e.getCurrentItem());
/* 266:198 */               p.getInventory().setItem(e.getSlot(), null);
/* 267:    */               
/* 268:200 */               noDisName = false;
/* 269:    */             }
/* 270:204 */             if (noDisName)
/* 271:    */             {
/* 272:205 */               boolean continu = true;
/* 273:206 */               if (e.getRawSlot() == 0) {
/* 274:207 */                 continu = false;
/* 275:    */               }
/* 276:209 */               if (continu)
/* 277:    */               {
/* 278:210 */                 ItemStack buffer = e.getCurrentItem();
/* 279:211 */                 p.getInventory().setItem(e.getSlot(), inv.getItem(0));
/* 280:212 */                 inv.setItem(0, buffer);
/* 281:    */               }
/* 282:    */             }
/* 283:215 */             ItemMeta me1 = inv.getItem(8).getItemMeta();
/* 284:216 */             Object lore = me1.getLore();
/* 285:217 */             String[] ss = ((String)((List)lore).get(0)).split(":");
/* 286:218 */             BigDecimal price = new BigDecimal(ss[1].trim());
/* 287:219 */             if (price.doubleValue() > 0.0D)
/* 288:    */             {
/* 289:220 */               me1.setLore(Arrays.asList(new String[] { ss[0].trim() + ": 0.00" }));
/* 290:221 */               inv.getItem(8).setItemMeta(me1);
/* 291:    */             }
/* 292:    */           }
/* 293:    */           catch (Exception e5)
/* 294:    */           {
/* 295:222 */             e5.printStackTrace();
/* 296:    */           }
/* 297:    */         }
/* 298:    */       }
/* 299:226 */       if (is != null) {
/* 300:227 */         is.setItemMeta(me);
/* 301:    */       }
/* 302:    */     }
/* 303:    */   }
/* 304:    */   
/* 305:    */   @EventHandler(priority=EventPriority.MONITOR)
/* 306:    */   public void onInventoryClose(InventoryCloseEvent e)
/* 307:    */   {
/* 308:232 */     if ((!e.getInventory().getTitle().toLowerCase().contains("item³")) || 
/* 309:233 */       (!e.getInventory().getTitle().toLowerCase().contains("sell"))) {
/* 310:234 */       return;
/* 311:    */     }
/* 312:236 */     if ((e.getInventory().getItem(0).getItemMeta().getDisplayName() != null) && 
/* 313:237 */       (e.getInventory().getItem(0).getItemMeta().getDisplayName().contains("³"))) {
/* 314:238 */       return;
/* 315:    */     }
/* 316:241 */     e.getPlayer().getLocation().getWorld().dropItemNaturally(e.getPlayer().getLocation(), e.getInventory().getItem(0));
/* 317:    */   }
/* 318:    */   
/* 319:    */   @EventHandler
/* 320:    */   public void onItemClick(InventoryClickEvent e)
/* 321:    */   {
/* 322:245 */     if (e.getInventory().getTitle().toLowerCase().contains("item³")) {
/* 323:246 */       e.setCancelled(true);
/* 324:    */     }
/* 325:    */   }
/* 326:    */ }


/* Location:           C:\Users\Ernest\Desktop\ItemCubed.jar
 * Qualified Name:     com.rockycraft.plugins.grandexchange.ButtonHandler
 * JD-Core Version:    0.7.0.1
 */